let listOfAllKnownAuthors = [];

class BookStore
{
    constructor(name, address, owner)
    {
        this._name = name;
        this._address = address;
        this._owner = owner;
        this._booksAvailable = [];
        this._totalCopiesOfAllBooks = 0
    }

    authorKnown(authorName)
    {
        let foundThem = false;
        //intializing ability to find book as false
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            //check each book in bookstore with author wanted
            if (authorName === listOfAllKnownAuthors[pos])
            {
                foundThem = true;
            }
        }
        return foundThem;
    }

    // This method adds a book to the store
    // Parameters:
    // bookInstance: the book class instance
    // copies: the number of books being added
    // Returns nothing 

    addBook(bookInstance, copies)
    {
        if (copies<0)
        {
            console.log("Can't add negative number of books");
            return;
        }
        let positionOfBook = this.checkForBook(bookInstance);
        if (positionOfBook != null)
        {//checking if book already exists
             let foundBook = this._booksAvailable[positionOfBook];
             foundBook.copies += copies; //add to existing copies
             console.log("Added " + copies + " copies of " + foundBook.book);
        }
        else
        {
             let bookCopies = {
                 book: bookInstance,
                 copies: copies
             };
             this._booksAvailable.push(bookCopies);
             console.log("Added " + copies + " copies of a new book: " + bookInstance);
        }

        this._totalCopiesOfAllBooks += copies;
    }

    // This method is called when a book is sold
    // Parameters: 
    // bookInstance: an instance of a book class
    // numberSold: number of that particular book sold
    sellBook(bookInstance, numberSold)
    {
        let positionOfBook = this.checkForBook(bookInstance);
        if (positionOfBook != null)
        {//check if book exists
            let foundBook = this._booksAvailable[positionOfBook];
            if (numberSold > this._booksAvailable[positionOfBook].copies)
            {//check if there are enough books to sell of this copy
                console.log("Not enough copies of " + foundBook.book + " to sell");
            }
            else
            {
                foundBook.copies -= numberSold;
                if (foundBook.copies === 0)
                {// if book sold is last book then update all variables
                    this._booksAvailable.pop(positionOfBook);
                    this._NumTitles -= 1;
                }
                this._totalCopiesOfAllBooks -= numberSold;
                console.log("Sold " + numberSold + " copies of " + foundBook.book);
            }
        }
        else
        {
            console.log(bookInstance + " not found");
        }
    }

    // This method checks to see whether the store has a particular book
    // Parameters: 
    // bookInstance: an instance of the book class
    // returns:
    // - nothing if the book is not in store
    // - the book number is the book is in store
    checkForBook(bookInstance)
    {//check the number of books available for one book
        let currBookNum = 0;
        while (currBookNum < this._booksAvailable.length)
        {
            if (this._booksAvailable[currBookNum].book.isTheSame(bookInstance))
            {
                return currBookNum;
            }
            currBookNum += 1;
        }
        return null;
    }

    get name()
    {
        return this._name;
    }

    set name(newName)
    {
        this._name = newName;
    }

    get address()
    {
        return this._address;
    }

    set address(newAddress)
    {
        this._address = newAddress;
    }

    get owner()
    {
        return this._owner;
    }

    set owner(newOwner)
    {
        this._owner = newOwner;
    }
}

class Book
{
    constructor(title, author, publicationYear, price)
    {
        this._title = title;
        this._author = author;
        this._publicationYear = publicationYear;
        this._price = price;
        if (this.authorKnown(this._author) === false)
        {
            listOfAllKnownAuthors.push(this._author)
        }
    }

    // This function sets the price of this book to the same price as another given book
    // Parameters: otherBook. a book class instance with a known price
    // Returns price of book
    isTheSame(otherBook)
    {
        return otherBook.price === this._price;
    }

    authorKnown(authorName)
    {
        let foundThem = false;
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            if (authorName === listOfAllKnownAuthors[pos])
            {
                foundThem = true;
            }
        }
        return foundThem;
    }

    get title()
    {
        return this._title;
    }

    get author()
    {
        return this._author;
    }

    get publicationYear()
    {
        return this._publicationYear;
    }

    get price()
    {
        return this._price;
    }
    
    toString()
    {
        return this._title + ", " + this._author + ". " + this._publicationYear + " ($" + this._price + ")";
    }
}

// Create new bookstore
let flourishAndBlotts = new BookStore("Flourish & Blotts", "North side, Diagon Alley, London, England", "unknown");
// Book details courtesy of Harry Potter series by J.K. Rowling
let cheapSpellBook = new Book("The idiot's guide to spells","Morlan",2005,40);
let monsterBook = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
let spellBook = new Book("The Standard Book of Spells, Grade 4", "Miranda Goshawk", 1921, 80);
// Add and Sell books
flourishAndBlotts.addBook(cheapSpellBook,1000);
flourishAndBlotts.addBook(monsterBook, 500);
flourishAndBlotts.sellBook(monsterBook, 200);
flourishAndBlotts.addBook(spellBook, 40);
flourishAndBlotts.addBook(spellBook, 20);
flourishAndBlotts.sellBook(spellBook, 15);
flourishAndBlotts.addBook(monsterBook, -30);
flourishAndBlotts.sellBook(monsterBook, 750);

console.log("Authors known: " + listOfAllKnownAuthors);
