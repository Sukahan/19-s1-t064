// Code for the main app page (locations list).
//define variables and activate functions to always show data
let cropsArray;
L.mapquest.key="OGmAd8izW3FTXq9oISbHIwp1vt1fYIT0";
displayLocations();
displayCrops();

//DarkSkyApi.apiKey = '42ecba05ab4dc5d9da7b788b385d41f8';

//function to view selected location
function viewLocation(locationName)
{
    // Save the desired location to local storage
    localStorage.setItem(APP_PREFIX + "-selectedLocation", locationName);
    // And load the view location page.
    location.href = 'viewLocation.html';
}


// function to display all locations in local storage
function displayLocations()
{
  // retrieve locations from local storage
  if (typeof(Storage) !== "undefined")
  {
      let JSONlocation = localStorage.getItem(LOC_STORAGE_KEY);
      locationArray = JSON.parse(JSONlocation);
  }
  else
  {
      console.log("Error: localStorage is not supported by current browser.");
  }
  // display locations using HTML string
  let locationListElement = document.getElementById('locationList');
  let locationListHTML = '';
  if (locationArray===null || locationArray.length===0)
  {
    //output for no locations entered
    locationListHTML+= '<span> &nbsp&nbsp&nbsp&nbsp; You currently have 0 locations in the database</span>';
  }
  else
  {
    for (let i=0; i<locationArray.length; i++)
    {
      locationObject = locationArray[i];
      locationInstance = new Location();
      locationInstance.initialiseFromLocationPDO(locationObject);
      getWeather(locationInstance);
      //create string to allow HTML to create locations list and display
      locationListHTML += '<li class="mdl-list__item mdl-list__item--two-line" onclick="viewLocation('+i+');">';
      locationListHTML += '<span class="mdl-list__item-primary-content">';
      locationListHTML +=  '<img class="mdl-list__item-icon" id="icon0" src="images/snow.png" class="list-avatar" />';
      if (locationInstance._nickname == "")
      {
          locationListHTML += '<span>'+ locationInstance._suburb+','+locationInstance._country+ '</span>';
      }
      else
      {
          locationListHTML += '<span>'+ locationInstance._nickname + '</span>';
      }
      locationListHTML += '<span class="mdl-list__item-sub-title">' + locationInstance._suburb + ", " + locationInstance._country + '</span>';
      locationListHTML += '</span>';
      locationListHTML += '<span class="mdl-list__item-secondary-content">';
      locationListHTML += '<span>'+ "Currently&#160;&#160;&#160;" + '</span>';
      locationListHTML += '<span>'+ "weatherInstance._currenttemp" + "&#176;C&#160;&#160;&#160;" + '</span>';
      locationListHTML += '</span>';

    }
  }
  //display location data
  locationListElement.innerHTML = locationListHTML;
}

// function to display all crops in local storage
function displayCrops()
{
  //retrieve Crops from local storage
  if (typeof(Storage) !== "undefined")
  {
      let JSONcrop = localStorage.getItem(CROP_STORAGE_KEY);
      cropsArray = JSON.parse(JSONcrop);
  }
  else
  {
      console.log("Error: localStorage is not supported by current browser.");
  }
  // display crops using HTML string
  let cropListElement = document.getElementById('cropList');
  let cropListHTML = '';
  if (cropsArray === null ||cropsArray.length === 0)
  {
    //output for no crops entered
    cropListHTML += '<span> &nbsp&nbsp&nbsp&nbsp; You currently have 0 crops in the database</span>';
  }
  else
  {
    for (let index=0; index<cropsArray.length; index++)
    {
      cropObject = cropsArray[index];
      cropInstance = new Crop();
      cropInstance.initialiseFromCropPDO(cropObject);
      //generate string to allow User to delete Crop from list and produce a pop up
      cropListHTML += '<li class="mdl-list__item mdl-list__item--two-line" id = "showDialog" onclick="dialogPopup('+index+')";>';
      cropListHTML += '<dialog class="mdl-dialog" id = "dialog">';
      cropListHTML += '<h4 class="mdl-dialog__title">Would you like to delete this crop?</h4>';
      cropListHTML += '<div class="mdl-dialog__content">';
      cropListHTML += '<p>Deleting will permanently remove the crop from your device.</p>';
      cropListHTML += '</div><div class="mdl-dialog__actions">';
      //generate string to allow HTML to create crop list and display
      cropListHTML += '<button type="button" class="mdl-button" onclick="deleteCrop()">Yes</button>';
      cropListHTML += '<button type="button" class="mdl-button close">No</button>'
      cropListHTML += '</div></dialog>';
      cropListHTML += '<span class="mdl-list__item-primary-content">';
      cropListHTML += '<img class="mdl-list__item-icon" id="icon0" src="images/crop.jpg" class="list-avatar" />';
      cropListHTML += '<span>'+ cropInstance._name +'</span>';
      cropListHTML += '<span class="mdl-list__item-sub-title">' + cropInstance._season + '</span>';
      cropListHTML += '</span></li>';
    }
  }
  //display crop data
  cropListElement.innerHTML = cropListHTML;
}


//pop up to confirm if user wants to delete crop from list
function dialogPopup(index)
{
  localStorage.setItem(APP_PREFIX + "-selectedCrop", index);
  let dialog = document.querySelector('dialog');
  if (! dialog.showModal)
  {
    dialogPolyfill.registerDialog(dialog);
  }
  dialog.showModal();
  dialog.querySelector('.close').addEventListener('click', function() {
    window.location.href="index.html";
  });
}

//Delete crop function
function deleteCrop()
{
  let index = localStorage.getItem(APP_PREFIX + "-selectedCrop");
  cropsArray.splice(index,1);
  localStorage.setItem(CROP_STORAGE_KEY,JSON.stringify(cropsArray));
  window.location.href="index.html";
}
/*for (let counter=0; counter<locationArray.length; counter++)
{
      let address = locationArray[counter].suburb+ ","+ locationArray[counter].state+","+locationArray[counter].country;
      // request location details from mapquest

}*/
