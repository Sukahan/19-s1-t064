//api.darksky.net/forecast/42ecba05ab4dc5d9da7b788b385d41f8;
// Code for LocationWeatherCache class and other shared code.

// Prefix to use for Local Storage.  You may change this.
// Defining variables
let APP_PREFIX = "weatherApp";
const CROP_STORAGE_KEY = APP_PREFIX+"Crops";
const LOC_STORAGE_KEY = APP_PREFIX+"Locations";
const WEATHER_STORAGE_KEY = APP_PREFIX+"Weather";
let today = new Date;
let todayString = today.simpleDateString();
let todayDarkSky = today.darkSkyDateString();

//jsonpRequest("", data);  //ADDED for jsonpRequest

class LocationWeatherCache //created a locationweathercache class
{
  constructor(location)
  {
    this._location = location;
  }
  set location(newLocation)
  {
    this._location = newlocation;
  }
  get location()
  {
    return this._location;
  }
}

let weatherCache = new LocationWeatherCache();
saveLocations(weatherCache);
// Restore the singleton locationWeatherCache from Local Storage.
function loadLocations()
{
  if (typeof(Storage) !== "undefined")
  {
      let JSONweather = localStorage.getItem(WEATHER_STORAGE_KEY);
      let weather = JSON.parse(JSONweather);
      return weather
  }
  else
  {
      console.log("Error: localStorage is not supported by current browser.");
  }
}

// Save the singleton locationWeatherCache to Local Storage.
function saveLocations(weatherCache)
{
  if (typeof(Storage) !== "undefined")
  {
		// stringify Location Instance
    let weatherJSON = JSON.stringify(weatherCache);
		localStorage.setItem(WEATHER_STORAGE_KEY,weatherJSON);
  }
  else
  {
    console.log("Error: localStorage is not supported by current browser.");
  }
}

// getting weather data
function getWeather(locationClass)
{
  let weatherObject = loadLocations();
  let data =
  {
      location:locationClass.suburb+ ","+ locationClass.state+","+locationClass.country,
      callback:"jsonpRequest"
  };
  let link = "http://www.mapquestapi.com/geocoding/v1/address?key=OGmAd8izW3FTXq9oISbHIwp1vt1fYIT0&";
  for (let key in data)
  {
    if (data.hasOwnProperty(key))
      {
        let encodedKey = encodeURIComponent(key);
        let encodedValue = encodeURIComponent(data[key]);
        if (encodedKey=="location")
        {
          link += encodedKey + "=" + encodedValue + "&";
        }
        else
        {
          link += encodedKey + "=" + encodedValue;
        }
      }
    }
  let script = document.createElement('script');
  script.src = link;
  document.body.appendChild(script);
}

function jsonpRequest(data)
{
    let info =
    {
      lat : data.results[0].locations[0].displayLatLng.lat,
      lng : data.results[0].locations[0].displayLatLng.lng,
      date : todayDarkSky,
      exclude : "hourly",
      callback : "getWeatherData"
    };
    //make object name
    propertyName = data.results[0].locations[0].adminArea3+ ", " +data.results[0].locations[0].adminArea1+info.date;
    localStorage.setItem(APP_PREFIX + "-objectProperty", propertyName);
    // Build URL parameters from data object.
    let params = "https://api.darksky.net/forecast/42ecba05ab4dc5d9da7b788b385d41f8/";
    // For each key in data object...
    for (let key in info)
    {
      if (info.hasOwnProperty(key))
        {
          let encodedKey = encodeURIComponent(key);
          let encodedValue = encodeURIComponent(info[key]);
          if (encodedKey != "callback"&&encodedKey != "exclude")
            {
              params += encodedValue;
              params += ",";
            }
          else if (encodedKey == "callback")
            {
              params += "&"+ encodedKey + "=" + encodedValue;
            }
          else
            {
              params = params.substring(0,params.length-1);
              params += "?"+ encodedKey + "=" + encodedValue;
            }
        }
    }
    let script = document.createElement('script');
    script.src = params;
    document.body.appendChild(script);
}

function getWeatherData(darkSkyData)
{
  let weatherData = loadLocations();
  console.log(weatherData);
  weatherData[localStorage.getItem(APP_PREFIX + "-objectProperty")] = darkSkyData.daily.data[0];
  saveLocations(weatherData);
  return weatherData;
}


class Crop //created a crop class
{
  constructor(name, season, minSafeTemp, maxSafeTemp, lowYieldOffset, tolerance)
  {
    this._name= name;
    this._season=season;
    this._minSafeTemp= minSafeTemp;
    this._maxSafeTemp= maxSafeTemp;
    this._lowYieldOffset= lowYieldOffset;
    this._tolerance= tolerance;

  }

  //public methods
  //SETTERS
  set name(newName)
  {
    this._name= newName;
  }

  set season(newSeason)
  {
    this._season= newSeason;
  }

  set minSafeTemp(newminSafeTemp)
  {
    this._minSafeTemp= newminSafeTemp;
  }
  set maxSafeTemp(newMaxSafeTemp)
  {
    this._maxSafeTemp= newMaxSafeTemp;
  }
  set lowYieldOffset(newLowYieldOffset)
  {
    this._lowYieldOffset= newLowYieldOffset;
  }
  set tolerance(newTolerance)
  {
    this._tolerance= newTolerance;
  }

  //GETTERS
  get name()
  {
    return this._name;
  }

  get season()
  {
    return this._season;
  }

  get minSafeTemp()
  {
    return this._minSafeTemp;
  }
  get maxSafeTemp()
  {
    return this._maxSafeTemp;
  }
  get lowYieldOffset()
  {
    return this._lowYieldOffset;
  }
  get tolerance()
  {
    return this._tolerance;
  }
  get all ()
   {
     return {
     name:this.name,
     season:this.season,
     minSafeTemp:this.minSafeTemp,
     maxSafeTemp:this.maxSafeTemp,
     lowYieldOffset:this.lowYieldOffset,
     tolerance:this.tolerance
   }
   }
  //re-intialises this instance from a public-data crop object.
  initialiseFromCropPDO(cropObject)
  {
    this.name = cropObject._name;
    this.season = cropObject._season;
    this.minSafeTemp= cropObject._minSafeTemp;
    this.maxSafeTemp= cropObject._maxSafeTemp;
    this.lowYieldOffset= cropObject._lowYieldOffset;
    this.tolerance= cropObject._tolerance;
  }

}

//Location class
class Location
{
  constructor (nickname, suburb, state, country)
  {
    this._nickname= nickname;
    this._suburb= suburb;
    this._state= state;
    this._country= country;
  }

  //public methods
  //GETTERS
   get nickname()
   {
      return this._nickname;
   }
   get suburb()
   {
     return this._suburb;
   }
   get state()
   {
     return this._state;
   }

   get country()
   {
     return this._country;
   }

  get all ()
   {
     return {
     nickname:this.nickname,
     suburb:this.suburb,
     state:this.state,
     country:this.country}
   }


   //SETTERS
   set nickname(newNickname)
   {
     this._nickname=newNickname;
   }
   set suburb(newSuburb)
   {
    this._suburb=newSuburb;
   }
   set state(newState)
   {
     this._state= newState;
   }

   set country(newCountry)
   {
    this._country=newCountry;
   }
   //re-intialises this instance from a public-data location object.
   initialiseFromLocationPDO(locationObject)
   {
    this.nickname = locationObject._nickname;
    this.suburb = locationObject._suburb;
    this.state= locationObject._state;
    this.country= locationObject._country;
   }
}

function seasons()
{
    
    season = 'unknown';
    switch(month) {
        case 'dec':
        case 'december':
        case '12':
        case 'jan':
        case 'january':
        case '1':
        case 'feb':
        case 'february':
        case '2':
            season = 'summer';
        break;
        case 'mar':
        case 'march':
        case '3':
        case 'apr':
        case 'april':
        case '4':
        case 'may':
        case '5':
            season = 'autumn';
        break;
        case 'jun':
        case 'june':
        case '6':
        case 'jul':
        case 'july':
        case '7':
        case 'aug':
        case 'august':
        case '8':
            season = 'winter';
        break;
        case 'sep':
        case 'september':
        case '9':
        case 'oct':
        case 'october':
        case '10':
        case 'nov':
        case 'november':
        case '11':
            season = 'spring';
        break;
    }
    alert(season);

}





/* {
         var i,
            icons = new Skycons({
                "color" : "#FFFFFF",
                "resizeClear": true // nasty android hack
            }),
            list  = [ // listing of all possible icons
                "clear-day",
                "clear-night",
                "partly-cloudy-day",
                "partly-cloudy-night",
                "cloudy",
                "rain",
                "sleet",
                "snow",
                "wind",
                "fog"
            ];

    // loop thru icon list array
    for(i = list.length; i--;) {
        var weatherType = list[i], // select each icon from list array
                // icons will have the name in the array above attached to the
                // canvas element as a class so let's hook into them.
                elements    = document.getElementsByClassName( weatherType );

        // loop thru the elements now and set them up
        for (e = elements.length; e--;) {
            icons.set(elements[e], weatherType);
        }
    } */
