// Code for the View Location page.

// This is sample code to demonstrate navigation.
// You need not use it for final app.

// Define variables
let locationIndex = localStorage.getItem(APP_PREFIX + "-selectedLocation");
let jsonLocations = localStorage.getItem(LOC_STORAGE_KEY);
let selectedLocation = null;
let marker;
let popup;
let latLng;
displayCropsData();

// key for creating view location map
mapboxgl.accessToken ="pk.eyJ1IjoiZW5nMTAwM3QwNjQiLCJhIjoiY2p1MjJhcGhtMDgwbzQ0cGdiYjB2d2Z6dyJ9.W-M-hC7eAJSaVfcNNhq6bA";
// key for obtaining lat and long for view location map
L.mapquest.key="OGmAd8izW3FTXq9oISbHIwp1vt1fYIT0";
// retrieve location from local storage
if(typeof(jsonLocations) != "undefined")
{
	if (typeof(locationIndex) !== 'undefined')
	{
		var selectedJson = JSON.parse(jsonLocations)[locationIndex];
		// create location selected instance
		selectedLocation = new Location();
		selectedLocation.initialiseFromLocationPDO(selectedJson);
	    document.getElementById("headerBarTitle").textContent = selectedLocation.nickname+"-location";
	}
}

// default map start point
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 10,
    center:[144.9631, -37.8136]
});

// create string from location selected to produce map
let address = selectedLocation.suburb+ ","+ selectedLocation.state+","+selectedLocation.country;// Punctuation must be used to separate.

// request location details from mapquest
L.mapquest.geocoding().geocode(address, createMap);

// function to create map on view location page
function createMap(error, response)
{
  let location = response.results[0].locations[0];
  latLng = location.displayLatLng;
  L.mapquest.geocoding().reverse(latLng, generatePopupContent);
}

//function to create location pop up info
function generatePopupContent(error, response)
{
	// obtain data from mapquest location info
  let location = response.results[0].locations[0];
  let street = location.street;
  let suburb = selectedLocation.suburb;
	let state = location.adminArea3;
  let country = location.adminArea1;
  let realAddress = selectedLocation.nickname + "<br/>" + street + ", " + suburb + "<br/>" + state + ", " + country;
  marker = new mapboxgl.Marker().setLngLat(latLng);
  popup = new mapboxgl.Popup({
    offset:45
  }).setLngLat(latLng);

  popup.setHTML(realAddress);
  marker.addTo(map);
  popup.addTo(map);
  map.flyTo({center: latLng});
}

//  display Crop Data in table relevant to location
function displayCropsData()
{
  //retrieve Crops from local storage
  if (typeof(Storage) !== "undefined")
  {
      let JSONcrop = localStorage.getItem(CROP_STORAGE_KEY);
      cropsArray = JSON.parse(JSONcrop);
  }
  else
  {
      console.log("Error: localStorage is not supported by current browser.");
  }
  //display crop data
  let cropDataElement = document.getElementById('cropData');
  let cropDataHTML = '';
  if (cropsArray === null ||cropsArray.length === 0)
  {
    cropDataHTML += '<span>You currently have 0 crops in the database</span>';
  }
  else
  {
    for (let i=0; i<cropsArray.length; i++)
    {
      cropObject = cropsArray[i];
      cropInstance = new Crop();
      cropInstance.initialiseFromCropPDO(cropObject);
			//create string to allow HTML to create crops data and display in table
      cropDataHTML += '<tr><td class="mdl-data-table__cell--non-numeric">'+ cropInstance._name+ '</td>';
			cropDataHTML += '<td>' + cropInstance._minSafeTemp+ '-'+ cropInstance._maxSafeTemp + '</td>';
			cropDataHTML += '<td> Dark </td> <td> sky </td><td> info </td></tr>'
    }
  }
	//display crop data
  cropDataElement.innerHTML = cropDataHTML;
}

//function to delete location from local storage
function deleteLocation(locationIndex)
{
  let locationArray= JSON.parse(jsonLocations);
	locationArray.splice(locationIndex,1);
  localStorage.setItem(LOC_STORAGE_KEY, JSON.stringify(locationArray));
	alert('You have successfully deleted location from local storage')
	window.location.href="index.html";
}

// function generateTableContent(crops, )
function getDate()
{
	let date = document.getElementById('dateInput').value;
	let dateFormatted = new Date(date);
	if ((dateFormatted.getTime() <= today.getTime()) && (dateFormatted.getTime() >= yearBeforeToday.getTime()))
	{
		let dateString = date.simpleDateString();
		let dateDarkSky = date.darkSkyDateString();
	}
	else
	{
		alert('Date input error please input date within range');
	}
return date
}
