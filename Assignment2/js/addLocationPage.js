// Code for the Add Location page.
// Defining variables
let locations = new Array(0);
let storageLocations = localStorage.getItem(LOC_STORAGE_KEY);
//key for creating 'check location' map
mapboxgl.accessToken ="pk.eyJ1IjoiZW5nMTAwM3QwNjQiLCJhIjoiY2p1MjJhcGhtMDgwbzQ0cGdiYjB2d2Z6dyJ9.W-M-hC7eAJSaVfcNNhq6bA";
// key for obtaining lat and long for 'check location' map
L.mapquest.key="OGmAd8izW3FTXq9oISbHIwp1vt1fYIT0";
// default map start point
let map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v10',
    zoom: 10,
    center:[144.9631, -37.8136]
});

if(typeof(storageLocations) != "undefined" && null !=storageLocations)
{
	//parse JSON string to array
	locations = JSON.parse(storageLocations);
}

//function to create Location class and store Location Data
function storeData()
{
  let nickname = document.getElementById("nickname").value;
  let suburb = document.getElementById("suburb").value;
	let state = document.getElementById("state").value;
  let country = document.getElementById("country").value;
	if (suburb===""||state===""||country==="")
	{
		let messageRef=document.getElementById("message");
		messageRef.innerHTML = "if the location is wrong, please input in all the fields."
	}
	//if (nickname==="")
	//create Location Instance
  if (nickname=="")
  {
    nickname = suburb+","+state+","+country;
  }
  let dataInstance = new Location(nickname, suburb, state, country);
  if (typeof(Storage) !== "undefined")
  {
		// stringify Location Instance
    locations.push(dataInstance)
    let dataInstanceJSON = JSON.stringify(locations);
		localStorage.setItem(LOC_STORAGE_KEY,dataInstanceJSON);

  }
  else
  {
    console.log("Error: localStorage is not supported by current browser.");
  }
  dataInstance = null;
};
function newaddress()
{
	window.location.href="addLocation.html";
}
// function for user to check that location is right
function checkLocation()
{
	let checkAddress= document.getElementById("suburb").value +','+ document.getElementById("state").value +','+ document.getElementById("country").value;
  L.mapquest.geocoding().geocode(checkAddress, createMap);
  suburb.disabled=true;
  state.disabled=true;
  country.disabled=true;
}

// function to create map for checking
function createMap(error, response)
{
  let location = response.results[0].locations[0];
  latLng = location.displayLatLng;
  L.mapquest.geocoding().reverse(latLng, generatePopupContent);
}

// function to allow details of location to pop up
function generatePopupContent(error, response)
{
  let location = response.results[0].locations[0];
  let street = location.street;
  let suburb = document.getElementById("suburb").value;
	let state = location.adminArea3;
  let country = location.adminArea1;
  let realAddress = document.getElementById("nickname").value + "<br/>" + street + ", " + suburb + "<br/>" + state + ", " + country;
  marker = new mapboxgl.Marker().setLngLat(latLng);
  popup = new mapboxgl.Popup({
    offset:45
  }).setLngLat(latLng);

  popup.setHTML(realAddress);
  marker.addTo(map);
  popup.addTo(map);
  map.flyTo({center: latLng});
}

//function to add more locations
function addLocation()
{
	storeData()
	alert("New location successfully added to Locations list")
	window.location.href="addLocation.html";
};
