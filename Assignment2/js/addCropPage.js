// Code for the Add Crop page.
// Defining variables
let crops= new Array(0);
let storageCrops = localStorage.getItem(CROP_STORAGE_KEY);
if(typeof(storageCrops) != "undefined" && null !=storageCrops)
{
	//parse JSON string to array
	crops = JSON.parse(storageCrops);
}

//function to create Crop class and store Crop Data
function storeCropData()
{
  let cropName = document.getElementById("cropName").value;
  let minTemp = document.getElementById("minTemp").value;
  let maxTemp = document.getElementById("maxTemp").value;
  let season = document.getElementById("season").value;
  let lowYieldOffset= document.getElementById("LYO").value;
  let tolerance = document.getElementById("tolerance").value;
	//make sure that max Temp is higher than min Temp
	if (Number(maxTemp) < Number(minTemp))
	{
		alert('INPUT ERROR: The maximum temperature needs to be bigger than the minimum temperature.')
		return false;
	}
	//create Crop Instance
  let cropInstance= new Crop(cropName, season, minTemp, maxTemp, lowYieldOffset, tolerance);
  if (typeof(Storage) !== "undefined")
    {
			//stringify crop Instance
      crops.push(cropInstance);
      let cropInstanceJSON= JSON.stringify(crops);
      localStorage.setItem(CROP_STORAGE_KEY, cropInstanceJSON);
    }
  else
    {
        console.log("Error: localStorage is not supported by current browser.");
    }
    cropInstance = null;
	return true;
};


//function to add more crops
function addCrop()
{
  if (storeCropData())
	{
		alert("New crop successfully added to Crops list");
  }
	window.location.href="addCrop.html";
};
